FIRSTNAME : Norman; Ruben;
LASTNAME : Lopez; Baeza;
UTEID :  nel349; rb27735;
CSACCOUNT : nel349; rb27735;
EMAIL : noell.lpz@utexas.edu; ruben.baeza@utexas.edu;

Throughput:
	Encoding:
		Encoded 11 MB file in 16 seconds. Throughput of 678.7264 KBps which is 0.66281875 MBps.
	Decoding:
		Decoded 11 MB file in 16 seconds. Throughput of 678.7264 KBps which is 0.66281875 MBps.
	
	Files used are included. 

Notes:
	We used the MixColumns implementation provided by Professor Young found here: http://www.cs.utexas.edu/~byoung/cs361/mixColumns-cheat-sheet
	
	To view information after each round and function set debug boolean to true.
	
	To view timing set time boolean to true.

Known Bugs:
None
	